﻿namespace Meep.Tech.Data {

  public partial class Universe {

    /// <summary>
    /// A Type that can be added as context to a universe.
    /// </summary>
    public class ExtraContext {}
  }
}
